#!/bin/bash

apt update
apt install docker.io docker-compose git

docker build -t web-server .
docker ps -aq | xargs docker stop | xargs docker rm
docker-compose up -d


#Leállító parancs
#docker ps -aq | xargs docker stop | xargs docker rm
