Ubuntu Linux telepítése után az alábbi parancsok kerültek futtatásra:

`sudo su`\
`apt update`\
`apt install docker.io docker-compose git`

Miután elkészült az index.html fájlom, illetve a kep.jpg-t is Google-ről letöltöttem az alábbi parancsot futtattam, hogy a **web-server** nevű Docker image-m elkészüljön:

`docker build -t web-server .`

Az image elkészülte után a futtatáshoz az alábbi parancsot használtam:

`docker run -d -p 80:80 --name web-server web-server`

Ez a 80-as porton fogja futtatni az elkészült alkalmazást.

A **docker-compose.yml** fájl megírása után az alábbi paranccsal tudtam a containert futtatni:\
`docker-compose up -d`

Innentől kezdve az alábbi portokon lehet elérni a különböző szolgáltatásokat (services):

- 8009 AJP (liferaydxp) 
- 8010 AJP (liferaydxp2)
- 80 (web-server)

UPDATE: Az eddigieket manuálisan tudtam futtatni. Az automatizáláshoz bash script került felhasználásra **automation.sh** fájl. Lehet explicit módon futtatni `sh automation.sh` . Az automatizáláshoz több megoldás létezik én a crontab -ot választottam. A crontab konfig fájlnak a végére a következő sort illesztettem be @reboot projekt_utvonala/automation.sh . Ezzel megvan adva az elérési útvonal is.
