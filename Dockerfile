FROM httpd:latest
COPY . /usr/local/apache2/htdocs/
CMD sed -i "s/<!--timestamp-->/Container started at $(date '+%Y-%m-%d %H:%M:%S')/" /usr/local/apache2/htdocs/index.html && httpd-foreground
